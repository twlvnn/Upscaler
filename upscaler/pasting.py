# pasting.py: Read images from a clipboard
#
# Copyright (C) 2023 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

from __future__ import annotations

from gettext import gettext as _
from typing import Callable, cast

from gi.repository import Adw, Gdk, Gio

from .window import UpscalerWindow


class Paster:
    def paste_file(
        self, parent_window: UpscalerWindow, callback: Callable[[Gio.File], None]
    ) -> None:
        self.parent_window = parent_window
        self.callback = callback

        clipboard = cast(Gdk.Display, Gdk.Display.get_default()).get_clipboard()
        clipboard.read_value_async(Gio.File, 0, None, self.__on_file_pasted)

    def __on_file_pasted(self, clipboard: Gdk.Clipboard, result: Gio.Task) -> None:
        # TODO: Have proper if-else statements instead of use try-except.
        try:
            paste_as_file = clipboard.read_value_finish(result)
            self.callback(paste_as_file)

        except:
            clipboard.read_texture_async(None, self.__on_texture_pasted)

    def __on_texture_pasted(self, clipboard: Gdk.Clipboard, result: Gio.Task) -> None:
        try:
            paste_as_texture = clipboard.read_texture_finish(result)
            save_file = Gio.File.new_tmp()[0]
            save_path = cast(str, save_file.get_path())
            cast(Gdk.Texture, paste_as_texture).save_to_png(save_path)
            self.callback(save_file)

        except:
            message = _("No image found in clipboard")
            self.parent_window.toast.add_toast(Adw.Toast.new(message))
