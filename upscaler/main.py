# main.py: main application
#
# Copyright (C) 2022 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

import gi

gi.require_version("Gtk", "4.0")
gi.require_version("Adw", "1")

import logging
import sys
from gettext import gettext as _
from typing import Any, Callable, Optional, cast

from gi.repository import Adw, Gio, GLib, Gtk

from upscaler.app_profile import (  # type: ignore
    APP_ID,
    APP_NAME,
    PROFILE,
)
from upscaler.utils import open_file_in_external_program
from upscaler.pasting import Paster
from upscaler.window import UpscalerWindow


class UpscalerApplication(Adw.Application):
    def __init__(self) -> None:
        super().__init__(
            application_id=APP_ID,
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
            resource_base_path="/io/gitlab/theevilskeleton/Upscaler",
        )
        self.create_action("quit", self.__quit, ["<primary>q"])
        self.create_action("about", self.__about_action)
        self.create_action("open", self.__open_file, ["<primary>o"])
        self.create_action("paste-image", self.__paste_file, ["<primary>v"])
        self.create_action(
            "open-output", self.__open_output, param=GLib.VariantType("s")
        )
        self.file: Optional[str] = None

    def do_activate(self) -> None:
        """
        Call when the application is activated.

        We raise the application's main window, creating it if
        necessary.
        """
        win: UpscalerWindow = cast(UpscalerWindow, self.props.active_window)
        if not win:
            win = UpscalerWindow(application=self)
        win.present()

        if PROFILE == "development":
            logging.getLogger().setLevel(logging.INFO)
            win.add_css_class("devel")

        if self.file is not None:
            logging.info(self.file)
            win.on_load_file(Gio.File.new_for_path(self.file))

    def __open_file(self, *args: Any) -> None:
        cast(UpscalerWindow, self.props.active_window).open_file()

    def __paste_file(self, *args: Any) -> None:
        win = cast(UpscalerWindow, self.props.active_window)
        Paster().paste_file(win, win.on_load_file)

    def __open_output(self, action: Gio.SimpleAction, data: GLib.Variant) -> None:
        file_path: str = data.unpack()
        open_file_in_external_program(file_path)

    def __about_action(self, *args: Any) -> None:
        """Open About window."""
        about = Adw.AboutDialog.new_from_appdata(
            "/io/gitlab/theevilskeleton/Upscaler/io.gitlab.theevilskeleton.Upscaler.metainfo.xml",
            "1.2.0",
        )
        about.set_developers(developers_list())
        about.set_artists(["Frostbitten-jello https://github.com/Frostbitten-jello"])
        about.set_copyright(_("Copyright © 2022 Upscaler Contributors"))
        about.set_translator_credits(_("translator-credits"))

        NCNN = '<a href="{url}" title="{url}">Upscayl NCNN</a>'.format(
            url="https://github.com/upscayl/upscayl-ncnn"
        )
        course = '<a href="{url}" title="{url}">CS50x</a>'.format(
            url="https://www.edx.org/course/introduction-computer-science-harvardx-cs50x"
        )

        about.set_comments(
            _(
                "<b><big>Backend</big></b>\n"
                "{app_name} uses {NCNN} under the hood to upscale and enhance images. Without it, {app_name} wouldn’t exist.\n\n"
                "<b><big>Origins of {app_name}</big></b>\n"
                "{app_name} started out as a final project for the final {course} assignment, Harvard University’s introductory course to computer science. However, it was later decided to continue it as free and open-source software. Without CS50x, {app_name} wouldn’t exist either."
            ).format(app_name=APP_NAME, NCNN=NCNN, course=course)
        )

        algorithms_sources = [
            "Real-ESRGAN https://github.com/xinntao/Real-ESRGAN",
            "Real-ESRGAN ncnn Vulkan https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan",
            "Upscayl-NCNN https://github.com/upscayl/upscayl-ncnn",
        ]
        about.add_acknowledgement_section(_("Algorithms by"), algorithms_sources)

        inspirations = [
            "Avvie https://github.com/Taiko2k/Avvie",
            "Bottles https://github.com/bottlesdevs/Bottles",
            "Loupe https://gitlab.gnome.org/BrainBlasted/loupe",
            "Totem https://gitlab.gnome.org/GNOME/totem",
            "Fractal https://gitlab.gnome.org/GNOME/fractal",
            "Builder https://gitlab.gnome.org/GNOME/gnome-builder",
        ]
        about.add_acknowledgement_section(
            _("Code and Design Borrowed from"), inspirations
        )

        sample_image = [
            "Princess Hinghoi https://safebooru.org/index.php?page=post&s=view&id=3084434",
        ]
        about.add_acknowledgement_section(_("Sample Image from"), sample_image)
        about.add_legal_section(
            title="Upscayl-NCNN",
            copyright=_("Copyright © 2024 Upscayl"),
            license_type=Gtk.License.AGPL_3_0,
            license=None,
        )
        about.add_legal_section(
            title="Real-ESRGAN ncnn Vulkan",
            copyright=_("Copyright © 2021 Xintao Wang"),
            license_type=Gtk.License.MIT_X11,
            license=None,
        )
        about.present(self.props.active_window)

    def create_action(
        self,
        name: str,
        callback: Callable[[Gio.SimpleAction, GLib.Variant], None],
        shortcuts: Optional[list[str]] = None,
        param: Optional[GLib.VariantType] = None,
    ) -> None:
        """
        Add an application action.

        Args:
            name: the name of the action
            callback: the function to be called when the action is
              activated
            shortcuts: an optional list of accelerators
            param: an optional list of parameters for the action
        """
        action = Gio.SimpleAction.new(name, param)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)

    def do_command_line(self, command_line: Gio.ApplicationCommandLine) -> int:
        args = command_line.get_arguments()
        if len(args) > 1:
            self.file = command_line.create_file_for_arg(args[1]).get_path()
        self.activate()
        return 0

    def __quit(self, _args: Any, *args: Any) -> None:
        """Quit application."""
        if (win := self.get_active_window()) is not None:
            win.destroy()


def developers_list() -> list[str]:
    """Developers/Contributors list. If you have contributed code, feel free
        to add yourself into the Python list:

    Name only:    \nHari Rana
    Name + URL:   \nHari Rana https://tesk.page
    Name + Email: \nHari Rana <theevilskeleton@riseup.net>
    """
    return [
        "Hari Rana (TheEvilSkeleton) https://tesk.page",
        "Matteo",
        "Onuralp SEZER <thunderbirdtr@fedoraproject.org>",
        "Khaleel Al-Adhami <khaleel.aladhami@gmail.com>",
        "gregorni https://gitlab.gnome.org/gregorni",
        "Clocks https://doomsdayrs.page",
    ]


def main(version: str) -> int:
    # The application's entry point.
    app = UpscalerApplication()
    return app.run(sys.argv)
