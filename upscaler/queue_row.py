# queue_row.py: Manage queue system.
#
# Copyright (C) 2024 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

from __future__ import annotations
import subprocess
import logging
from gettext import gettext as _
from typing import Any, Optional

from PIL import Image  # type: ignore

from gi.repository import Adw, Gdk, GObject, Gtk

from upscaler.utils import create_texture_from_img


@Gtk.Template(resource_path="/io/gitlab/theevilskeleton/Upscaler/gtk/queue-row.ui")
class QueueRow(Adw.PreferencesRow):
    __gtype_name__ = "QueueRow"

    thumbnail: Gtk.Picture = Gtk.Template.Child()  # type: ignore
    progressbar: Gtk.ProgressBar = Gtk.Template.Child()  # type: ignore
    button_cancel: Gtk.Button = Gtk.Template.Child()  # type: ignore
    button_open: Gtk.Button = Gtk.Template.Child()  # type: ignore

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        self.process: subprocess.Popen[Any]
        self.input_file_path: Optional[str] = None
        self.output_file_path: Optional[str] = None
        self.selected_model: Optional[str] = None
        self.cancelled: bool = False
        self.command: Optional[list[str]] = None
        self.texture: Optional[Gdk.Texture] = None

        self.button_cancel.connect("clicked", self.__on_cancel_clicked_cb)
        self.progressbar.connect("notify::fraction", self.__on_progressbar_updated_cb)

        self.set_css_name("queuerow")

    @property
    def input_file_path(self) -> Optional[str]:
        return self._input_file_path

    @input_file_path.setter
    def input_file_path(self, path: Optional[str]) -> None:
        if path is None:
            self._input_file_path = None
            return

        if self._input_file_path == path:
            return

        self._input_file_path = path

        if self.texture:
            self.thumbnail.set_paintable(self.texture)
            return

        img = Image.open(path)

        self.thumbnail.set_paintable(create_texture_from_img(img))

    def run(self) -> None:
        """Run command if @self.command is set."""
        if self.command:
            self.button_cancel.set_tooltip_text(_("Stop"))
            self.process = subprocess.Popen(
                self.command, stderr=subprocess.PIPE, universal_newlines=True
            )

    @GObject.Signal
    def aborted(self):  # type: ignore
        logging.info("Signal 'abort' emitted")

    def __on_cancel_clicked_cb(self, *args: Any) -> None:
        if hasattr(self, "process"):
            title = _("Stop Upscaling?")
            subtitle = _("All progress will be lost")
        else:
            title = _("Remove From Queue?")
            subtitle = _("Its order will be lost")

        dialog = Adw.AlertDialog.new(
            title,
            subtitle,
        )

        def response(dialog: Adw.AlertDialog, response_id: str) -> None:
            if response_id == "stop":
                self.emit("aborted")

        dialog.add_response("cancel", _("_Cancel"))
        dialog.add_response("stop", _("_Stop"))
        dialog.set_response_appearance("stop", Adw.ResponseAppearance.DESTRUCTIVE)
        dialog.connect("response", response)
        dialog.present(self)

    def __on_progressbar_updated_cb(
        self, progressbar: Gtk.ProgressBar, *args: Any
    ) -> None:
        percentage = progressbar.get_fraction() * 100
        progressbar.set_text(_(f"{percentage:.0f}%"))
