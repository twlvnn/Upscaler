# utils.py: utility classes and functions
#
# Copyright (C) 2024 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

from __future__ import annotations

from gettext import gettext as _

from gi.repository import Adw, Gdk, Gio, GLib, Gtk

import logging
from os.path import basename
from typing import Any, Callable, Optional
from PIL import Image  # type: ignore


# Formats
image_formats = ["image/png", "image/jpeg", "image/webp"]
video_formats = ["video/mp4"]


def set_formats(formats: list[str]) -> Gtk.FileFilter:
    """Set filter."""
    filter = Gtk.FileFilter()
    for format in formats:
        filter.add_mime_type(format)

    filter.set_name(_("Supported image files"))
    return filter


def supported_filters() -> Gtk.FileFilter:
    """Return all supported formats."""
    # filter = set_formats(image_formats + video_formats)
    filter = set_formats(image_formats)
    return filter


def image_filters() -> Gtk.FileFilter:
    """Return image specific filters."""
    filter = set_formats(image_formats)
    return filter


def create_texture_from_img(img: Image) -> Gdk.Texture:
    """Create a Gdk.Texture from a PIL.Image."""
    img = img.convert("RGBA")

    width, height = img.size

    texture: Gdk.Texture = Gdk.MemoryTexture.new(
        width,
        height,
        Gdk.MemoryFormat.R8G8B8A8,
        GLib.Bytes.new(img.tobytes()),
        # "Stride" is the amount of bytes per row in a given image.
        # The R8G8B8A8 format is 4 bytes long (each channel is a byte (8-bits)), so we multiply the width by 4 to obtain the stride.
        width * 4,
    )

    return texture


def open_file_in_external_program(file_path: str, *args: Any) -> None:
    """Open file in external program."""
    file = open(file_path, "r")
    fid = file.fileno()
    connection = Gio.bus_get_sync(Gio.BusType.SESSION, None)
    proxy = Gio.DBusProxy.new_sync(
        connection,
        Gio.DBusProxyFlags.NONE,
        None,
        "org.freedesktop.portal.Desktop",
        "/org/freedesktop/portal/desktop",
        "org.freedesktop.portal.OpenURI",
        None,
    )

    try:
        proxy.call_with_unix_fd_list_sync(
            "OpenFile",
            GLib.Variant("(sha{sv})", ("", 0, {"ask": GLib.Variant("b", True)})),
            Gio.DBusCallFlags.NONE,
            -1,
            Gio.UnixFDList.new_from_array([fid]),
            None,
        )
    except Exception as e:
        logging.error(f"Error: {e}")


# File chooser wrapper
class FileChooser:
    @staticmethod
    def open_file(
        parent: Adw.ApplicationWindow, on_load_file: Callable[[Gio.File], None]
    ) -> None:
        """Open an image file from a file chooser dialog."""

        def load_file(
            _dialog: Gtk.FileChooserNative, response: Gtk.ResponseType
        ) -> None:
            # Return if user cancels
            if response != Gtk.ResponseType.ACCEPT:
                return

            # Run if the user selects an image
            if (file := dialog.get_file()) is not None:
                on_load_file(file)

        dialog = Gtk.FileChooserNative.new(
            title=_("Select an image"),
            parent=parent,
            action=Gtk.FileChooserAction.OPEN,
            accept_label=None,
            cancel_label=None,
        )
        dialog.set_modal(True)
        dialog.connect("response", load_file)
        dialog.add_filter(supported_filters())
        dialog.show()

    @staticmethod
    def output_file(
        parent: Adw.ApplicationWindow,
        default_name: str,
        good: Callable[[str], None],
        bad: Callable[[Optional[str]], None],
        *args: Any,
    ) -> None:
        """Select output location from file chooser dialog."""

        def upscale_content(
            _dialog: Gtk.FileChooserNative, response: Gtk.ResponseType
        ) -> None:
            # Set output file path if user selects a location
            if response != Gtk.ResponseType.ACCEPT:
                bad(None)
                return

            # Check if output file has a file extension or format is supported
            file = dialog.get_file()
            if file is None:
                bad(None)
                return

            output_file_path = file.get_path()
            if output_file_path is None:
                bad(None)
                return

            if "." not in basename(output_file_path):
                bad(_("No file extension was specified"))
                return

            elif Gio.content_type_guess(output_file_path)[0] not in image_formats:
                filename = basename(output_file_path).split(".").pop()
                bad(
                    _("“{filename}” is an unsupported format").format(filename=filename)
                )
                return

            logging.info(f"Output file: {output_file_path}")
            good(output_file_path)

        dialog = Gtk.FileChooserNative.new(
            title=_("Select output location"),
            parent=parent,
            action=Gtk.FileChooserAction.SAVE,
            accept_label=None,
            cancel_label=None,
        )
        dialog.set_modal(True)
        dialog.connect("response", upscale_content)
        dialog.add_filter(image_filters())
        dialog.set_current_name(default_name)
        dialog.show()
